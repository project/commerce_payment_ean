<?php

/**
 * @file
 * Provides a payment for electronic payments and collecting EAN number.
 */


/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_payment_ean_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_payment_ean'] = array(
    'title' => t('Electronic invoice (EAN)'),
    'description' => t('Electronic invoice collects EAN number in the payment process.'),
    'active' => FALSE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: submit form.
 */
function commerce_payment_ean_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form = array();

  // Merge in values from the order.
  if (!empty($order->data['commerce_payment_ean'])) {
    $pane_values += $order->data['commerce_payment_ean'];
  }

  // Merge in default values.
  $pane_values += array(
    'ean' => '',
  );

  $form['ean'] = array(
    '#type' => 'textfield',
    '#title' => t('EAN'),
    '#description' => t('Please supply the 13 digits long EAN number to direct the invoice to.'),
    '#default_value' => $pane_values['ean'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Payment method callback: submit form validation.
 */
function commerce_payment_ean_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  // Throw an error if the EAN number does not conform to standards.
  if (strlen($pane_values['ean']) != 13 AND !is_numeric($pane_values['ean'])) {
    form_set_error(implode('][', array_merge($form_parents, array('ean'))), t('You must enter 13 digits for the EAN number.'));

    // Even though the form error is enough to stop the submission of the form,
    // it's not enough to stop it from a Commerce standpoint because of the
    // combined validation / submission going on per-pane in the checkout form.
    return FALSE;
  }
}

/**
 * Payment method callback: submit form submission.
 */
function commerce_payment_ean_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order->data['commerce_payment_ean'] = $pane_values;

  commerce_payment_ean_transaction($payment_method, $order, $charge, $pane_values['ean']);
}

/**
 * Implements hook_field_access().
 */
function commerce_payment_ean_field_access($op, $field, $entity_type, $entity, $account) {
  if ($field['field_name'] == 'commerce_payment_ean') {
    // Only allow admins to edit field value.
    return user_access('configure store', $account);
  }
  return TRUE;
}

/**
 * Creates a payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 * @param $ean
 *   The ean entered on the submission form.
 */
function commerce_payment_ean_transaction($payment_method, $order, $charge, $ean) {
  $transaction = commerce_payment_transaction_new('commerce_payment_ean', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->message = 'EAN: @ean';
  $transaction->message_variables = array('@ean' => $ean);

  // Save the EAN number on the contact.
  commerce_payment_ean_save_ean_on_contact($order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id'], $ean);

  // Save transaction.
  commerce_payment_transaction_save($transaction);
}

function commerce_payment_ean_save_ean_on_contact($contact_id, $ean) {
  $contact = commerce_customer_profile_load($contact_id);
  $contact_wrapper = entity_metadata_wrapper('commerce_customer_profile', $contact);

  // Save id to the entity.
  $contact_wrapper->commerce_payment_ean->set($ean);
  $contact_wrapper->save();
}
